<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn_cancel</name>
   <tag></tag>
   <elementGuidId>d31fd2b4-1549-49f2-9f90-f06cf85eea7f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@class=&quot;ant-modal ant-modal-confirm ant-modal-confirm-confirm&quot;]/div[2]/div/div/div[2]/button[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@class=&quot;ant-modal ant-modal-confirm ant-modal-confirm-confirm&quot;]/div[2]/div/div/div[2]/button[1]</value>
   </webElementProperties>
</WebElementEntity>
