<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn_cancel</name>
   <tag></tag>
   <elementGuidId>83080ac8-090a-4be1-b6a4-10298bd09879</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@class=&quot;ant-modal ant-modal-confirm ant-modal-confirm-confirm&quot;]/div[2]/div/div/div[2]/button[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@class=&quot;ant-modal ant-modal-confirm ant-modal-confirm-confirm&quot;]/div[2]/div/div/div[2]/button[1]</value>
   </webElementProperties>
</WebElementEntity>
