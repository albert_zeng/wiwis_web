<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn_confirm</name>
   <tag></tag>
   <elementGuidId>6fde9417-1c30-439f-94b7-d372f0c43d28</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@class=&quot;ant-modal ant-modal-confirm ant-modal-confirm-confirm&quot;]/div[2]/div/div/div[2]/button[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@class=&quot;ant-modal ant-modal-confirm ant-modal-confirm-confirm&quot;]/div[2]/div/div/div[2]/button[2]</value>
   </webElementProperties>
</WebElementEntity>
