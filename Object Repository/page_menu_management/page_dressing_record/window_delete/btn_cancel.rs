<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn_cancel</name>
   <tag></tag>
   <elementGuidId>f2cccd85-b3d6-4fca-9611-1d520b07f140</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@class=&quot;ant-modal ant-modal-confirm ant-modal-confirm-confirm&quot;]/div[2]/div/div/div[2]/button[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@class=&quot;ant-modal ant-modal-confirm ant-modal-confirm-confirm&quot;]/div[2]/div/div/div[2]/button[1]</value>
   </webElementProperties>
</WebElementEntity>
