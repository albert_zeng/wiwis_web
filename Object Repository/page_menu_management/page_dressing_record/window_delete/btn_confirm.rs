<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn_confirm</name>
   <tag></tag>
   <elementGuidId>bcc2872b-bb3b-48c3-a4da-115aa4c9ba06</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@class=&quot;ant-modal ant-modal-confirm ant-modal-confirm-confirm&quot;]/div[2]/div/div/div[2]/button[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@class=&quot;ant-modal ant-modal-confirm ant-modal-confirm-confirm&quot;]/div[2]/div/div/div[2]/button[2]</value>
   </webElementProperties>
</WebElementEntity>
