# Installation

* Katalon_Studio_Windows_64-6.3.2

# IDE

* Katalon_Studio_Windows_64-6.3.2

# Available Scripts

In the Katalon directory, you can run:

### `katalon -noSplash  -runMode=console -projectPath="{project_directory_path}\WiWis_Web.prj" -retry=0 -testSuitePath="Test Suites/Daily_Test" -executionProfile="default" -browserType="Chrome" -apiKey=d2483b54-76d8-433a-8d3c-aaa9d1d3d229`

Running auto testing.

# CI/CD

Running on Shared Runners, we don't need to install Runner, please see [.gitlab-ci.yam](https://gitlab.devpack.cc/mr0s00/wiwis/wiwis_web/blob/master/.gitlab-ci.yml) for more information about config setting.