import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.testobject.ConditionType as ConditionType
import internal.GlobalVariable as GlobalVariable
import org.junit.After as After
import org.openqa.selenium.By as By
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import org.openqa.selenium.Keys as Keys
import org.openqa.selenium.interactions.Actions as Actions

WebDriver driver = DriverFactory.getWebDriver()

Actions action = new Actions(driver)

WebUI.setText(findTestObject('page_login/input_username'), GlobalVariable.username)

WebUI.setText(findTestObject('page_login/input_password'), GlobalVariable.password)

WebUI.click(findTestObject('page_login/btn_login'))

WebUI.delay(5)

WebUI.click(findTestObject('page_menu/btn_account'))

WebUI.delay(1)

//increase_app_account//
WebUI.click(findTestObject('page_account/tab_app_account'))

WebUI.delay(1)

WebUI.click(findTestObject('page_account/page_app_account/btn_increase_app_account'))

WebUI.delay(1)

WebUI.click(findTestObject('page_account/window_increase/btn_increase_cancel'))

WebUI.delay(1)

WebUI.click(findTestObject('page_account/page_app_account/btn_increase_app_account'))

WebUI.delay(1)

WebUI.verifyElementText(findTestObject('page_account/page_app_account/text_increase_label_division'), '科別')

WebUI.verifyElementText(findTestObject('page_account/page_app_account/text_increase_label_jobTitle'), '職稱')

WebUI.verifyElementText(findTestObject('page_account/page_app_account/text_increase_label_name'), '姓名')

WebUI.verifyElementText(findTestObject('page_account/page_app_account/droplist_increase_label_division'), '全部')

WebUI.verifyElementText(findTestObject('page_account/page_app_account/droplist_increase_label_jobTitle'), '全部')

WebUI.verifyElementText(findTestObject('page_account/page_app_account/droplist_increase_label_name'), '選擇姓名')

WebUI.click(findTestObject('page_account/page_app_account/droplist_increase_label_name'))

action.sendKeys(Keys.DOWN).sendKeys(Keys.ENTER).perform()

label_name = WebUI.getText(findTestObject('page_account/page_app_account/droplist_increase_label_name'))

first_label_name = label_name

while (!(label_name.equals(app_account))) {
    WebUI.click(findTestObject('page_account/page_app_account/droplist_increase_label_name'))

    action.sendKeys(Keys.DOWN).sendKeys(Keys.ENTER).perform()
	
    label_name = WebUI.getText(findTestObject('page_account/page_app_account/droplist_increase_label_name'))
	
	if (label_name.equals(first_label_name)) {
		WebUI.verifyMatch(false, true, false)
		
		break
		
	}
}

WebUI.verifyElementText(findTestObject('page_account/page_app_account/droplist_increase_label_name'), app_account)

WebUI.click(findTestObject('page_account/window_increase/btn_increase_confirm'))

WebUI.delay(1)

WebUI.verifyElementText(findTestObject('page_account/dialog_message'), '新增成功')

WebUI.delay(1)

List<WebElement> app_accountListTable = driver.findElements(By.xpath('//*[@id="APP登入人員"]/div/div[1]/div[1]/table/tbody/tr'))

value = false

for (WebElement pointerValue : app_accountListTable) {
    if (pointerValue.getText().contains(app_account)) {
        value = true

        break
    }
}

WebUI.verifyEqual(value, true)

WebUI.delay(1)

WebUI.setText(findTestObject('page_account/page_app_account/input_search_app_account'), app_account_division)

WebUI.click(findTestObject('page_account/page_app_account/btn_search_app_account'))

WebUI.delay(1)

app_accountListTable = driver.findElements(By.xpath('//*[@id="APP登入人員"]/div/div[1]/div[1]/table/tbody/tr'))

for (WebElement pointerValue : app_accountListTable) {
    WebUI.verifyEqual(pointerValue.getText().contains(app_account_division), true)
}

WebUI.setText(findTestObject('page_account/page_app_account/input_search_app_account'), app_account_job_title)

WebUI.click(findTestObject('page_account/page_app_account/btn_search_app_account'))

WebUI.delay(1)

app_accountListTable = driver.findElements(By.xpath('//*[@id="APP登入人員"]/div/div[1]/div[1]/table/tbody/tr'))

for (WebElement pointerValue : app_accountListTable) {
    WebUI.verifyEqual(pointerValue.getText().contains(app_account_job_title), true)
}

WebUI.setText(findTestObject('page_account/page_app_account/input_search_app_account'), app_account_name)

WebUI.click(findTestObject('page_account/page_app_account/btn_search_app_account'))

WebUI.delay(1)

app_accountListTable = driver.findElements(By.xpath('//*[@id="APP登入人員"]/div/div[1]/div[1]/table/tbody/tr'))

for (WebElement pointerValue : app_accountListTable) {
    WebUI.verifyEqual(pointerValue.getText().contains(app_account_name), true)
}

WebUI.refresh()

WebUI.delay(2)

//increase_web_account//
WebUI.click(findTestObject('page_account/tab_web_account'))

WebUI.delay(1)

WebUI.click(findTestObject('page_account/page_web_account/btn_increase_web_account'))

WebUI.delay(1)

WebUI.click(findTestObject('page_account/window_increase/btn_increase_cancel'))

WebUI.delay(1)

WebUI.click(findTestObject('page_account/page_web_account/btn_increase_web_account'))

WebUI.delay(1)

WebUI.verifyElementText(findTestObject('page_account/page_web_account/text_increase_label_division'), '科別')

WebUI.verifyElementText(findTestObject('page_account/page_web_account/text_increase_label_jobTitle'), '職稱')

WebUI.verifyElementText(findTestObject('page_account/page_web_account/text_increase_label_name'), '姓名')

WebUI.verifyElementText(findTestObject('page_account/page_web_account/droplist_increase_label_division'), '全部')

WebUI.verifyElementText(findTestObject('page_account/page_web_account/droplist_increase_label_jobTitle'), '全部')

WebUI.verifyElementText(findTestObject('page_account/page_web_account/droplist_increase_label_name'), '選擇姓名')

WebUI.click(findTestObject('page_account/page_web_account/droplist_increase_label_name'))

action.sendKeys(Keys.DOWN).sendKeys(Keys.ENTER).perform()

label_name = WebUI.getText(findTestObject('page_account/page_web_account/droplist_increase_label_name'))

first_label_name = label_name

while (!(label_name.equals(web_account))) {
	WebUI.click(findTestObject('page_account/page_web_account/droplist_increase_label_name'))

	action.sendKeys(Keys.DOWN).sendKeys(Keys.ENTER).perform()
	
	label_name = WebUI.getText(findTestObject('page_account/page_web_account/droplist_increase_label_name'))
	
	if (label_name.equals(first_label_name)) {
		WebUI.verifyMatch(false, true, false)
		
		break
		
	}
}

WebUI.verifyElementText(findTestObject('page_account/page_web_account/droplist_increase_label_name'), web_account)

WebUI.click(findTestObject('page_account/window_increase/btn_increase_confirm'))

WebUI.delay(1)

WebUI.verifyElementText(findTestObject('page_account/dialog_message'), '新增成功')

WebUI.delay(1)

List<WebElement> web_accountListTable = driver.findElements(By.xpath('//*[@id="後台WEB登入管理者"]/div/div[1]/div[1]/table/tbody/tr'))

value = false

for (WebElement pointerValue : web_accountListTable) {
	if (pointerValue.getText().contains(web_account)) {
		value = true

		break
	}
}

WebUI.verifyEqual(value, true)

WebUI.delay(1)

WebUI.setText(findTestObject('page_account/page_web_account/input_search_web_account'), web_account_division)

WebUI.click(findTestObject('page_account/page_web_account/btn_search_web_account'))

WebUI.delay(1)

web_accountListTable = driver.findElements(By.xpath('//*[@id="後台WEB登入管理者"]/div/div[1]/div[1]/table/tbody/tr'))

for (WebElement pointerValue : web_accountListTable) {
	WebUI.verifyEqual(pointerValue.getText().contains(web_account_division), true)
}

WebUI.setText(findTestObject('page_account/page_web_account/input_search_web_account'), web_account_job_title)

WebUI.click(findTestObject('page_account/page_web_account/btn_search_web_account'))

WebUI.delay(1)

web_accountListTable = driver.findElements(By.xpath('//*[@id="後台WEB登入管理者"]/div/div[1]/div[1]/table/tbody/tr'))

for (WebElement pointerValue : web_accountListTable) {
	WebUI.verifyEqual(pointerValue.getText().contains(web_account_job_title), true)
}

WebUI.setText(findTestObject('page_account/page_web_account/input_search_web_account'), web_account_name)

WebUI.click(findTestObject('page_account/page_web_account/btn_search_web_account'))

WebUI.delay(1)

web_accountListTable = driver.findElements(By.xpath('//*[@id="後台WEB登入管理者"]/div/div[1]/div[1]/table/tbody/tr'))

for (WebElement pointerValue : web_accountListTable) {
	WebUI.verifyEqual(pointerValue.getText().contains(web_account_name), true)
}

WebUI.refresh()

WebUI.delay(2)

//decrease_app_account//
WebUI.click(findTestObject('page_account/tab_app_account'))

WebUI.delay(1)

app_accountListTable = driver.findElements(By.xpath('//*[@id="APP登入人員"]/div/div[1]/div[1]/table/tbody/tr'))

app_accountButtonTable = driver.findElements(By.xpath('//*[@id="APP登入人員"]/div/div[2]/div/div/table/tbody/tr'))

i = 0

for (WebElement pointerValue : app_accountListTable) {
    if (pointerValue.getText().contains(app_account)) {
        app_accountButtonTable.get(i).findElement(By.xpath('./td/div/div/div[1]/button')).click()

        WebUI.delay(1)

        app_accountButtonTable.get(i).findElement(By.xpath('./td/div/div/div[2]/div/div/div/button')).click()

        WebUI.delay(2)
		
		WebUI.click(findTestObject('page_account/window_delete/btn_cancel'))
		
		WebUI.delay(1)
		
		app_accountButtonTable.get(i).findElement(By.xpath('./td/div/div/div[1]/button')).click()
		
		WebUI.delay(1)
		
		app_accountButtonTable.get(i).findElement(By.xpath('./td/div/div/div[2]/div/div/div/button')).click()
		
		WebUI.delay(2)

        WebUI.click(findTestObject('page_account/window_delete/btn_confirm'))

        WebUI.delay(1)

        WebUI.verifyElementText(findTestObject('page_account/dialog_message'), '刪除成功')

        break
    }
    
    i++
}

WebUI.refresh()

WebUI.delay(2)

//decrease_web_account//
WebUI.click(findTestObject('page_account/tab_web_account'))

WebUI.delay(1)

web_accountListTable = driver.findElements(By.xpath('//*[@id="後台WEB登入管理者"]/div/div[1]/div[1]/table/tbody/tr'))

web_accountButtonTable = driver.findElements(By.xpath('//*[@id="後台WEB登入管理者"]/div/div[2]/div/div/table/tbody/tr'))

i = 0

for (WebElement pointerValue : web_accountListTable) {
	if (pointerValue.getText().contains(web_account)) {
		web_accountButtonTable.get(i).findElement(By.xpath('./td/div/div/div[1]/button')).click()

		WebUI.delay(1)

		web_accountButtonTable.get(i).findElement(By.xpath('./td/div/div/div[2]/div/div/div/button')).click()

		WebUI.delay(2)
		
		WebUI.click(findTestObject('page_account/window_delete/btn_cancel'))
		
		WebUI.delay(1)
		
		web_accountButtonTable.get(i).findElement(By.xpath('./td/div/div/div[1]/button')).click()
		
		WebUI.delay(1)
		
		web_accountButtonTable.get(i).findElement(By.xpath('./td/div/div/div[2]/div/div/div/button')).click()
		
		WebUI.delay(2)

		WebUI.click(findTestObject('page_account/window_delete/btn_confirm'))

		WebUI.delay(1)

		WebUI.verifyElementText(findTestObject('page_account/dialog_message'), '刪除成功')

		break
	}
	
	i++
}