import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.setText(findTestObject('page_login/input_username'), GlobalVariable.username)

WebUI.setText(findTestObject('page_login/input_password'), GlobalVariable.password)

WebUI.click(findTestObject('page_login/btn_login'))

WebUI.delay(5)

WebUI.click(findTestObject('page_menu/btn_account'))

WebUI.delay(1)

WebUI.verifyMatch(WebUI.getUrl(), GlobalVariable.url + 'account', false)

WebUI.click(findTestObject('page_account/tab_web_account'))

WebUI.delay(1)

WebUI.verifyElementText(findTestObject('page_account/page_web_account/title_name'), '姓名')

WebUI.verifyElementText(findTestObject('page_account/page_web_account/title_jobTitle'), '職稱')

WebUI.verifyElementText(findTestObject('page_account/page_web_account/title_division'), '科別')

WebUI.verifyElementText(findTestObject('page_account/page_web_account/title_account'), '帳號')

WebUI.verifyElementText(findTestObject('page_account/page_web_account/title_updateTime'), '更新時間')

WebUI.click(findTestObject('page_account/tab_app_account'))

WebUI.delay(1)

WebUI.verifyElementText(findTestObject('page_account/page_app_account/title_name'), '姓名')

WebUI.verifyElementText(findTestObject('page_account/page_app_account/title_jobTitle'), '職稱')

WebUI.verifyElementText(findTestObject('page_account/page_app_account/title_division'), '科別')

WebUI.verifyElementText(findTestObject('page_account/page_app_account/title_account'), '帳號')

WebUI.verifyElementText(findTestObject('page_account/page_app_account/title_updateTime'), '更新時間')

