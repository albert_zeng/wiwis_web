import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.testobject.ConditionType as ConditionType
import internal.GlobalVariable as GlobalVariable
import org.junit.After as After
import org.openqa.selenium.By as By
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import org.openqa.selenium.Keys as Keys
import org.openqa.selenium.interactions.Actions as Actions

WebDriver driver = DriverFactory.getWebDriver()

Actions action = new Actions(driver)

WebUI.setText(findTestObject('page_login/input_username'), GlobalVariable.username)

WebUI.setText(findTestObject('page_login/input_password'), GlobalVariable.password)

WebUI.click(findTestObject('page_login/btn_login'))

WebUI.delay(5)

WebUI.click(findTestObject('page_menu/btn_advisory_setting'))

WebUI.delay(1)

//increase_wound_specialist//
WebUI.click(findTestObject('page_advisory_setting/tab_wound_specialist'))

WebUI.delay(1)

WebUI.click(findTestObject('page_advisory_setting/page_wound_specialist/btn_increase_wound_specialist'))

WebUI.delay(1)

WebUI.click(findTestObject('page_advisory_setting/window_increase/btn_increase_cancel'))

WebUI.delay(1)

WebUI.click(findTestObject('page_advisory_setting/page_wound_specialist/btn_increase_wound_specialist'))

WebUI.delay(1)

WebUI.verifyElementText(findTestObject('page_advisory_setting/page_wound_specialist/text_increase_label_division'), '科別')

WebUI.verifyElementText(findTestObject('page_advisory_setting/page_wound_specialist/text_increase_label_jobTitle'), '職稱')

WebUI.verifyElementText(findTestObject('page_advisory_setting/page_wound_specialist/text_increase_label_name'), '姓名')

WebUI.verifyElementText(findTestObject('page_advisory_setting/page_wound_specialist/droplist_increase_label_division'), 
    '全部')

WebUI.verifyElementText(findTestObject('page_advisory_setting/page_wound_specialist/droplist_increase_label_jobTitle'), 
    '醫師')

WebUI.verifyElementAttributeValue(findTestObject('page_advisory_setting/page_wound_specialist/droplist_increase_label_jobTitle'), 
    'class', 'ant-select ant-select-disabled', 30)

WebUI.verifyElementText(findTestObject('page_advisory_setting/page_wound_specialist/droplist_increase_label_name'), '選擇姓名')

WebUI.click(findTestObject('page_advisory_setting/page_wound_specialist/droplist_increase_label_name'))

action.sendKeys(Keys.DOWN).sendKeys(Keys.ENTER).perform()

label_name = WebUI.getText(findTestObject('page_advisory_setting/page_wound_specialist/droplist_increase_label_name'))

first_label_name = label_name

while (!(label_name.equals(woundSpecialist))) {
    WebUI.click(findTestObject('page_advisory_setting/page_wound_specialist/droplist_increase_label_name'))

    action.sendKeys(Keys.DOWN).sendKeys(Keys.ENTER).perform()
	
    label_name = WebUI.getText(findTestObject('page_advisory_setting/page_wound_specialist/droplist_increase_label_name'))
	
	if (label_name.equals(first_label_name)) {
		WebUI.verifyMatch(false, true, false)
		
		break
		
	}
}

WebUI.verifyElementText(findTestObject('page_advisory_setting/page_wound_specialist/droplist_increase_label_name'), woundSpecialist)

WebUI.click(findTestObject('page_advisory_setting/window_increase/btn_increase_confirm'))

WebUI.delay(1)

WebUI.verifyElementText(findTestObject('page_advisory_setting/dialog_message'), '新增成功')

WebUI.delay(1)

List<WebElement> wound_specialistListTable = driver.findElements(By.xpath('//*[@class="patient-info"]/div/div[3]/div[1]/div/div[2]/div/div/div/div/div[1]/div[1]/table/tbody/tr'))

value = false

for (WebElement pointerValue : wound_specialistListTable) {
    if (pointerValue.getText().contains(woundSpecialist)) {
        value = true

        break
    }
}

WebUI.verifyEqual(value, true)

WebUI.delay(1)

WebUI.setText(findTestObject('page_advisory_setting/page_wound_specialist/input_search_wound_specialist'), woundSpecialist_division)

WebUI.click(findTestObject('page_advisory_setting/page_wound_specialist/btn_search_wound_specialist'))

WebUI.delay(1)

wound_specialistListTable = driver.findElements(By.xpath('//*[@class="patient-info"]/div/div[3]/div[1]/div/div[2]/div/div/div/div/div[1]/div[1]/table/tbody/tr'))

for (WebElement pointerValue : wound_specialistListTable) {
    WebUI.verifyEqual(pointerValue.getText().contains(woundSpecialist_division), true)
}

WebUI.setText(findTestObject('page_advisory_setting/page_wound_specialist/input_search_wound_specialist'), woundSpecialist_job_title)

WebUI.click(findTestObject('page_advisory_setting/page_wound_specialist/btn_search_wound_specialist'))

WebUI.delay(1)

wound_specialistListTable = driver.findElements(By.xpath('//*[@class="patient-info"]/div/div[3]/div[1]/div/div[2]/div/div/div/div/div[1]/div[1]/table/tbody/tr'))

for (WebElement pointerValue : wound_specialistListTable) {
    WebUI.verifyEqual(pointerValue.getText().contains(woundSpecialist_job_title), true)
}

WebUI.setText(findTestObject('page_advisory_setting/page_wound_specialist/input_search_wound_specialist'), woundSpecialist_name)

WebUI.click(findTestObject('page_advisory_setting/page_wound_specialist/btn_search_wound_specialist'))

WebUI.delay(1)

wound_specialistListTable = driver.findElements(By.xpath('//*[@class="patient-info"]/div/div[3]/div[1]/div/div[2]/div/div/div/div/div[1]/div[1]/table/tbody/tr'))

for (WebElement pointerValue : wound_specialistListTable) {
    WebUI.verifyEqual(pointerValue.getText().contains(woundSpecialist_name), true)
}

WebUI.refresh()

WebUI.delay(2)

//increase_doctor//
WebUI.click(findTestObject('page_advisory_setting/tab_doctor'))

WebUI.delay(1)

WebUI.click(findTestObject('page_advisory_setting/page_doctor/btn_increase_doctor'))

WebUI.delay(1)

WebUI.click(findTestObject('page_advisory_setting/window_increase/btn_increase_cancel'))

WebUI.delay(1)

WebUI.click(findTestObject('page_advisory_setting/page_doctor/btn_increase_doctor'))

WebUI.delay(1)

WebUI.verifyElementText(findTestObject('page_advisory_setting/page_doctor/text_increase_label_division'), '科別')

WebUI.verifyElementText(findTestObject('page_advisory_setting/page_doctor/text_increase_label_jobTitle'), '職稱')

WebUI.verifyElementText(findTestObject('page_advisory_setting/page_doctor/text_increase_label_name'), '姓名')

WebUI.verifyElementText(findTestObject('page_advisory_setting/page_doctor/text_increase_label_consultant_division'), '向上諮詢科別')

WebUI.verifyElementText(findTestObject('page_advisory_setting/page_doctor/text_increase_label_consultant_jobTitle'), '向上諮詢職稱')

WebUI.verifyElementText(findTestObject('page_advisory_setting/page_doctor/text_increase_label_consultant_name'), '向上諮詢姓名')

WebUI.verifyElementText(findTestObject('page_advisory_setting/page_doctor/droplist_increase_label_division'), '全部')

WebUI.verifyElementText(findTestObject('page_advisory_setting/page_doctor/droplist_increase_label_jobTitle'), '醫師')

WebUI.verifyElementAttributeValue(findTestObject('page_advisory_setting/page_doctor/droplist_increase_label_jobTitle'), 
    'class', 'ant-select ant-select-disabled', 30)

WebUI.verifyElementText(findTestObject('page_advisory_setting/page_doctor/droplist_increase_label_name'), '選擇姓名')

WebUI.verifyElementText(findTestObject('page_advisory_setting/page_doctor/droplist_increase_label_consultant_division'), 
    '全部')

WebUI.verifyElementText(findTestObject('page_advisory_setting/page_doctor/droplist_increase_label_consultant_jobTitle'), 
    '醫師')

WebUI.verifyElementAttributeValue(findTestObject('page_advisory_setting/page_doctor/droplist_increase_label_consultant_jobTitle'), 
    'class', 'ant-select ant-select-disabled', 30)

WebUI.verifyElementText(findTestObject('page_advisory_setting/page_doctor/droplist_increase_label_consultant_name'), '選擇姓名')

WebUI.click(findTestObject('page_advisory_setting/page_doctor/droplist_increase_label_name'))

action.sendKeys(Keys.DOWN).sendKeys(Keys.ENTER).perform()

label_name = WebUI.getText(findTestObject('page_advisory_setting/page_doctor/droplist_increase_label_name'))

first_label_name = label_name

while (!(label_name.equals(doctor))) {
    WebUI.click(findTestObject('page_advisory_setting/page_doctor/droplist_increase_label_name'))

    action.sendKeys(Keys.DOWN).sendKeys(Keys.ENTER).perform()

    label_name = WebUI.getText(findTestObject('page_advisory_setting/page_doctor/droplist_increase_label_name'))
	
	if (label_name.equals(first_label_name)) {
		WebUI.verifyMatch(false, true, false)
		
		break
		
	}
}

WebUI.verifyElementText(findTestObject('page_advisory_setting/page_doctor/droplist_increase_label_name'), doctor)

WebUI.click(findTestObject('page_advisory_setting/page_doctor/droplist_increase_label_consultant_name'))

action.sendKeys(Keys.DOWN).sendKeys(Keys.ENTER).perform()

label_name = WebUI.getText(findTestObject('page_advisory_setting/page_doctor/droplist_increase_label_consultant_name'))

first_label_name = label_name

while (!(label_name.equals(woundSpecialist))) {
    WebUI.click(findTestObject('page_advisory_setting/page_doctor/droplist_increase_label_consultant_name'))

    action.sendKeys(Keys.DOWN).sendKeys(Keys.ENTER).perform()

    label_name = WebUI.getText(findTestObject('page_advisory_setting/page_doctor/droplist_increase_label_consultant_name'))
	
	if (label_name.equals(first_label_name)) {
		WebUI.verifyMatch(false, true, false)
		
		break
		
	}
}

WebUI.verifyElementText(findTestObject('page_advisory_setting/page_doctor/droplist_increase_label_consultant_name'), woundSpecialist)

WebUI.click(findTestObject('page_advisory_setting/window_increase/btn_increase_confirm'))

WebUI.delay(1)

WebUI.verifyElementText(findTestObject('page_advisory_setting/dialog_message'), '新增成功')

WebUI.delay(1)

List<WebElement> doctorListTable = driver.findElements(By.xpath('//*[@class="patient-info"]/div/div[3]/div[2]/div/div[2]/div/div/div/div/div[1]/div[1]/table/tbody/tr'))

value = false

for (WebElement pointerValue : doctorListTable) {
    if (pointerValue.getText().contains(doctor)) {
        value = true

        WebUI.verifyEqual(pointerValue.findElement(By.xpath('./td[4]')).getText(), woundSpecialist_name)

        break
    }
}

WebUI.verifyEqual(value, true)

WebUI.delay(1)

WebUI.setText(findTestObject('page_advisory_setting/page_doctor/input_search_doctor'), doctor_division)

WebUI.click(findTestObject('page_advisory_setting/page_doctor/btn_search_doctor'))

WebUI.delay(1)

doctorListTable = driver.findElements(By.xpath('//*[@class="patient-info"]/div/div[3]/div[2]/div/div[2]/div/div/div/div/div[1]/div[1]/table/tbody/tr'))

for (WebElement pointerValue : doctorListTable) {
    WebUI.verifyEqual(pointerValue.getText().contains(doctor_division), true)
}

WebUI.setText(findTestObject('page_advisory_setting/page_doctor/input_search_doctor'), doctor_job_title)

WebUI.click(findTestObject('page_advisory_setting/page_doctor/btn_search_doctor'))

WebUI.delay(1)

doctorListTable = driver.findElements(By.xpath('//*[@class="patient-info"]/div/div[3]/div[2]/div/div[2]/div/div/div/div/div[1]/div[1]/table/tbody/tr'))

for (WebElement pointerValue : doctorListTable) {
    WebUI.verifyEqual(pointerValue.getText().contains(doctor_job_title), true)
}

WebUI.setText(findTestObject('page_advisory_setting/page_doctor/input_search_doctor'), doctor_name)

WebUI.click(findTestObject('page_advisory_setting/page_doctor/btn_search_doctor'))

WebUI.delay(1)

doctorListTable = driver.findElements(By.xpath('//*[@class="patient-info"]/div/div[3]/div[2]/div/div[2]/div/div/div/div/div[1]/div[1]/table/tbody/tr'))

for (WebElement pointerValue : doctorListTable) {
    WebUI.verifyEqual(pointerValue.getText().contains(doctor_name), true)
}
WebUI.refresh()

WebUI.delay(2)

//increase_case_manager//
WebUI.click(findTestObject('page_advisory_setting/tab_case_manager'))

WebUI.delay(1)

WebUI.click(findTestObject('page_advisory_setting/page_case_manager/btn_increase_case_manager'))

WebUI.delay(1)

WebUI.click(findTestObject('page_advisory_setting/window_increase/btn_increase_cancel'))

WebUI.delay(1)

WebUI.click(findTestObject('page_advisory_setting/page_case_manager/btn_increase_case_manager'))

WebUI.delay(1)

WebUI.verifyElementText(findTestObject('page_advisory_setting/page_case_manager/text_increase_label_division'), '科別')

WebUI.verifyElementText(findTestObject('page_advisory_setting/page_case_manager/text_increase_label_jobTitle'), '職稱')

WebUI.verifyElementText(findTestObject('page_advisory_setting/page_case_manager/text_increase_label_name'), '姓名')

WebUI.verifyElementText(findTestObject('page_advisory_setting/page_case_manager/text_increase_label_consultant_division'), 
    '向上諮詢科別')

WebUI.verifyElementText(findTestObject('page_advisory_setting/page_case_manager/text_increase_label_consultant_jobTitle'), 
    '向上諮詢職稱')

WebUI.verifyElementText(findTestObject('page_advisory_setting/page_case_manager/text_increase_label_consultant_name'), '向上諮詢姓名')

WebUI.verifyElementText(findTestObject('page_advisory_setting/page_case_manager/droplist_increase_label_division'), '全部')

WebUI.verifyElementText(findTestObject('page_advisory_setting/page_case_manager/droplist_increase_label_jobTitle'), '專科護理師')

WebUI.verifyElementAttributeValue(findTestObject('page_advisory_setting/page_case_manager/droplist_increase_label_jobTitle'), 
    'class', 'ant-select ant-select-disabled', 30)

WebUI.verifyElementText(findTestObject('page_advisory_setting/page_case_manager/droplist_increase_label_name'), '選擇姓名')

WebUI.verifyElementText(findTestObject('page_advisory_setting/page_case_manager/droplist_increase_label_consultant_division'), 
    '全部')

WebUI.verifyElementText(findTestObject('page_advisory_setting/page_case_manager/droplist_increase_label_consultant_jobTitle'), 
    '醫師')

WebUI.verifyElementAttributeValue(findTestObject('page_advisory_setting/page_case_manager/droplist_increase_label_consultant_jobTitle'), 
    'class', 'ant-select ant-select-disabled', 30)

WebUI.verifyElementText(findTestObject('page_advisory_setting/page_case_manager/droplist_increase_label_consultant_name'), 
    '選擇姓名')

WebUI.click(findTestObject('page_advisory_setting/page_case_manager/droplist_increase_label_name'))

action.sendKeys(Keys.DOWN).sendKeys(Keys.ENTER).perform()

label_name = WebUI.getText(findTestObject('page_advisory_setting/page_case_manager/droplist_increase_label_name'))

first_label_name = label_name

while (!(label_name.equals(case_manager))) {
    WebUI.click(findTestObject('page_advisory_setting/page_case_manager/droplist_increase_label_name'))

    action.sendKeys(Keys.DOWN).sendKeys(Keys.ENTER).perform()
	
    label_name = WebUI.getText(findTestObject('page_advisory_setting/page_case_manager/droplist_increase_label_name'))
		
	if (label_name.equals(first_label_name)) {
		WebUI.verifyMatch(false, true, false)
		
		break
		
	}
}

WebUI.verifyElementText(findTestObject('page_advisory_setting/page_case_manager/droplist_increase_label_name'), case_manager)

WebUI.click(findTestObject('page_advisory_setting/page_case_manager/droplist_increase_label_consultant_name'))

action.sendKeys(Keys.DOWN).sendKeys(Keys.ENTER).perform()

label_name = WebUI.getText(findTestObject('page_advisory_setting/page_case_manager/droplist_increase_label_consultant_name'))

first_label_name = label_name

while (!(label_name.equals(woundSpecialist))) {
    WebUI.click(findTestObject('page_advisory_setting/page_case_manager/droplist_increase_label_consultant_name'))

    action.sendKeys(Keys.DOWN).sendKeys(Keys.ENTER).perform()

    label_name = WebUI.getText(findTestObject('page_advisory_setting/page_case_manager/droplist_increase_label_consultant_name'))
	
	if (label_name.equals(first_label_name)) {
		WebUI.verifyMatch(false, true, false)
		
		break
		
	}
}

WebUI.verifyElementText(findTestObject('page_advisory_setting/page_case_manager/droplist_increase_label_consultant_name'), 
    woundSpecialist)

WebUI.click(findTestObject('page_advisory_setting/window_increase/btn_increase_confirm'))

WebUI.delay(1)

WebUI.verifyElementText(findTestObject('page_advisory_setting/dialog_message'), '新增成功')

WebUI.delay(1)

List<WebElement> case_managerListTable = driver.findElements(By.xpath('//*[@class="patient-info"]/div/div[3]/div[3]/div/div[2]/div/div/div/div/div[1]/div[1]/table/tbody/tr'))

value = false

for (WebElement pointerValue : case_managerListTable) {
    if (pointerValue.getText().contains(case_manager)) {
        value = true

        WebUI.verifyEqual(pointerValue.findElement(By.xpath('./td[4]')).getText(), woundSpecialist_name)

        break
    }
}

WebUI.verifyEqual(value, true)

WebUI.setText(findTestObject('page_advisory_setting/page_case_manager/input_search_case_manager'), case_manager_division)

WebUI.click(findTestObject('page_advisory_setting/page_case_manager/btn_search_case_manager'))

WebUI.delay(1)

case_managerListTable = driver.findElements(By.xpath('//*[@class="patient-info"]/div/div[3]/div[3]/div/div[2]/div/div/div/div/div[1]/div[1]/table/tbody/tr'))

for (WebElement pointerValue : case_managerListTable) {
    WebUI.verifyEqual(pointerValue.getText().contains(case_manager_division), true)
}

WebUI.setText(findTestObject('page_advisory_setting/page_case_manager/input_search_case_manager'), case_manager_job_title)

WebUI.click(findTestObject('page_advisory_setting/page_case_manager/btn_search_case_manager'))

WebUI.delay(1)

case_managerListTable = driver.findElements(By.xpath('//*[@class="patient-info"]/div/div[3]/div[3]/div/div[2]/div/div/div/div/div[1]/div[1]/table/tbody/tr'))

for (WebElement pointerValue : case_managerListTable) {
    WebUI.verifyEqual(pointerValue.getText().contains(case_manager_job_title), true)
}

WebUI.setText(findTestObject('page_advisory_setting/page_case_manager/input_search_case_manager'), case_manager_name)

WebUI.click(findTestObject('page_advisory_setting/page_case_manager/btn_search_case_manager'))

WebUI.delay(1)

case_managerListTable = driver.findElements(By.xpath('//*[@class="patient-info"]/div/div[3]/div[3]/div/div[2]/div/div/div/div/div[1]/div[1]/table/tbody/tr'))

for (WebElement pointerValue : case_managerListTable) {
    WebUI.verifyEqual(pointerValue.getText().contains(case_manager_name), true)
}
WebUI.refresh()

WebUI.delay(2)

//increase_consultation_window//
WebUI.click(findTestObject('page_advisory_setting/tab_consultation_window'))

WebUI.delay(1)

WebUI.click(findTestObject('page_advisory_setting/page_consultation_window/btn_increase_consultation_window'))

WebUI.delay(1)

WebUI.click(findTestObject('page_advisory_setting/window_increase/btn_increase_cancel'))

WebUI.delay(1)

WebUI.click(findTestObject('page_advisory_setting/page_consultation_window/btn_increase_consultation_window'))

WebUI.delay(1)

WebUI.verifyElementText(findTestObject('page_advisory_setting/page_consultation_window/text_increase_label_division'), '科別')

WebUI.verifyElementText(findTestObject('page_advisory_setting/page_consultation_window/text_increase_label_jobTitle'), '職稱')

WebUI.verifyElementText(findTestObject('page_advisory_setting/page_consultation_window/text_increase_label_name'), '姓名')

WebUI.verifyElementText(findTestObject('page_advisory_setting/page_consultation_window/text_increase_label_consultant_division'), 
    '向上諮詢科別')

WebUI.verifyElementText(findTestObject('page_advisory_setting/page_consultation_window/text_increase_label_consultant_jobTitle'), 
    '向上諮詢職稱')

WebUI.verifyElementText(findTestObject('page_advisory_setting/page_consultation_window/text_increase_label_consultant_name'), '向上諮詢姓名')

WebUI.verifyElementText(findTestObject('page_advisory_setting/page_consultation_window/droplist_increase_label_division'), '全部')

WebUI.verifyElementText(findTestObject('page_advisory_setting/page_consultation_window/droplist_increase_label_jobTitle'), '專科護理師')

WebUI.verifyElementAttributeValue(findTestObject('page_advisory_setting/page_consultation_window/droplist_increase_label_jobTitle'), 
    'class', 'ant-select ant-select-disabled', 30)

WebUI.verifyElementText(findTestObject('page_advisory_setting/page_consultation_window/droplist_increase_label_name'), '選擇姓名')

WebUI.verifyElementText(findTestObject('page_advisory_setting/page_consultation_window/droplist_increase_label_consultant_division'), 
    '全部')

WebUI.verifyElementText(findTestObject('page_advisory_setting/page_consultation_window/droplist_increase_label_consultant_jobTitle'), 
    '醫師')

WebUI.verifyElementAttributeValue(findTestObject('page_advisory_setting/page_consultation_window/droplist_increase_label_consultant_jobTitle'), 
    'class', 'ant-select ant-select-disabled', 30)

WebUI.verifyElementText(findTestObject('page_advisory_setting/page_consultation_window/droplist_increase_label_consultant_name'), 
    '選擇姓名')

WebUI.click(findTestObject('page_advisory_setting/page_consultation_window/droplist_increase_label_name'))

action.sendKeys(Keys.DOWN).sendKeys(Keys.ENTER).perform()

label_name = WebUI.getText(findTestObject('page_advisory_setting/page_consultation_window/droplist_increase_label_name'))

first_label_name = label_name

while (!(label_name.equals(consultation_window))) {
    WebUI.click(findTestObject('page_advisory_setting/page_consultation_window/droplist_increase_label_name'))

    action.sendKeys(Keys.DOWN).sendKeys(Keys.ENTER).perform()
	
    label_name = WebUI.getText(findTestObject('page_advisory_setting/page_consultation_window/droplist_increase_label_name'))
	
	if (label_name.equals(first_label_name)) {
		WebUI.verifyMatch(false, true, false)
		
		break
		
	}
}

WebUI.verifyElementText(findTestObject('page_advisory_setting/page_consultation_window/droplist_increase_label_name'), consultation_window)

WebUI.click(findTestObject('page_advisory_setting/page_consultation_window/droplist_increase_label_consultant_name'))

action.sendKeys(Keys.DOWN).sendKeys(Keys.ENTER).perform()

label_name = WebUI.getText(findTestObject('page_advisory_setting/page_consultation_window/droplist_increase_label_consultant_name'))

first_label_name = label_name

while (!(label_name.equals(woundSpecialist))) {
	WebUI.click(findTestObject('page_advisory_setting/page_consultation_window/droplist_increase_label_consultant_name'))

	action.sendKeys(Keys.DOWN).sendKeys(Keys.ENTER).perform()

	label_name = WebUI.getText(findTestObject('page_advisory_setting/page_consultation_window/droplist_increase_label_consultant_name'))

	if (label_name.equals(first_label_name)) {
		WebUI.verifyMatch(false, true, false)
		
		break
		
	}
}

WebUI.verifyElementText(findTestObject('page_advisory_setting/page_consultation_window/droplist_increase_label_consultant_name'),
	woundSpecialist)

WebUI.verifyElementText(findTestObject('page_advisory_setting/page_consultation_window/droplist_increase_label_name'), consultation_window)

WebUI.click(findTestObject('page_advisory_setting/window_increase/btn_increase_confirm'))

WebUI.delay(1)

WebUI.verifyElementText(findTestObject('page_advisory_setting/dialog_message'), '新增成功')

WebUI.delay(1)

List<WebElement> consultation_windowListTable = driver.findElements(By.xpath('//*[@class="patient-info"]/div/div[3]/div[4]/div/div[2]/div/div/div/div/div[1]/div[1]/table/tbody/tr'))

value = false

for (WebElement pointerValue : consultation_windowListTable) {
    if (pointerValue.getText().contains(consultation_window)) {
        value = true

        break
    }
}

WebUI.verifyEqual(value, true)

WebUI.delay(1)

WebUI.setText(findTestObject('page_advisory_setting/page_consultation_window/input_search_consultation_window'), consultation_window_division)

WebUI.click(findTestObject('page_advisory_setting/page_consultation_window/btn_search_consultation_window'))

WebUI.delay(1)

consultation_windowListTable = driver.findElements(By.xpath('//*[@class="patient-info"]/div/div[3]/div[4]/div/div[2]/div/div/div/div/div[1]/div[1]/table/tbody/tr'))

for (WebElement pointerValue : consultation_windowListTable) {
	WebUI.verifyEqual(pointerValue.getText().contains(consultation_window_division), true)
}

WebUI.setText(findTestObject('page_advisory_setting/page_consultation_window/input_search_consultation_window'), consultation_window_job_title)

WebUI.click(findTestObject('page_advisory_setting/page_consultation_window/btn_search_consultation_window'))

WebUI.delay(1)

consultation_windowListTable = driver.findElements(By.xpath('//*[@class="patient-info"]/div/div[3]/div[4]/div/div[2]/div/div/div/div/div[1]/div[1]/table/tbody/tr'))

for (WebElement pointerValue : consultation_windowListTable) {
	WebUI.verifyEqual(pointerValue.getText().contains(consultation_window_job_title), true)
}

WebUI.setText(findTestObject('page_advisory_setting/page_consultation_window/input_search_consultation_window'), consultation_window_name)

WebUI.click(findTestObject('page_advisory_setting/page_consultation_window/btn_search_consultation_window'))

WebUI.delay(1)

consultation_windowListTable = driver.findElements(By.xpath('//*[@class="patient-info"]/div/div[3]/div[4]/div/div[2]/div/div/div/div/div[1]/div[1]/table/tbody/tr'))

for (WebElement pointerValue : consultation_windowListTable) {
	WebUI.verifyEqual(pointerValue.getText().contains(consultation_window_name), true)
}

WebUI.refresh()

WebUI.delay(3)

//decrease_wound_specialist//
WebUI.click(findTestObject('page_advisory_setting/tab_wound_specialist'))

WebUI.delay(1)

wound_specialistListTable = driver.findElements(By.xpath('//*[@class="patient-info"]/div/div[3]/div[1]/div/div[2]/div/div/div/div/div[1]/div[1]/table/tbody/tr'))
wound_specialistButtonTable = driver.findElements(By.xpath('//*[@class="patient-info"]/div/div[3]/div[1]/div[2]/div[2]/div/div/div/div/div[2]/div/div/table/tbody/tr'))

i = 0

for (WebElement pointerValue : wound_specialistListTable) {
	if (pointerValue.getText().contains(woundSpecialist)) {
		wound_specialistButtonTable.get(i).findElement(By.xpath('./td/div/div/div[1]/button')).click()

		WebUI.delay(1)

		wound_specialistButtonTable.get(i).findElement(By.xpath('./td/div/div/div[2]/div/div/div/button')).click()

		WebUI.delay(2)

		WebUI.click(findTestObject('page_advisory_setting/window_delete/btn_cancel'))

		WebUI.delay(1)
		
		wound_specialistButtonTable.get(i).findElement(By.xpath('./td/div/div/div[1]/button')).click()
		
		WebUI.delay(1)
		
		wound_specialistButtonTable.get(i).findElement(By.xpath('./td/div/div/div[2]/div/div/div/button')).click()
		
		WebUI.delay(2)
		
		WebUI.click(findTestObject('page_advisory_setting/window_delete/btn_confirm'))
		
		WebUI.delay(1)
		
		WebUI.verifyElementText(findTestObject('page_advisory_setting/dialog_message'), '無法刪除: 此位傷口專家已被其他醫師設定為向上諮詢對象，如要刪除，請先行解除此傷口專家被其他醫師設定為向上諮詢之身份，再回此頁進行刪除。')
		break
	}
	
	i++
}

WebUI.refresh()

WebUI.delay(2)

//decrease_doctor//
WebUI.click(findTestObject('page_advisory_setting/tab_doctor'))

WebUI.delay(1)

doctorListTable = driver.findElements(By.xpath('//*[@class="patient-info"]/div/div[3]/div[2]/div/div[2]/div/div/div/div/div[1]/div[1]/table/tbody/tr'))
doctorButtonTable = driver.findElements(By.xpath('//*[@class="patient-info"]/div/div[3]/div[2]/div[2]/div[2]/div/div/div/div/div[2]/div/div/table/tbody/tr'))

i = 0

for (WebElement pointerValue : doctorListTable) {
	if (pointerValue.getText().contains(doctor)) {
		doctorButtonTable.get(i).findElement(By.xpath('./td/div/div/div[1]/button')).click()

		WebUI.delay(1)

		doctorButtonTable.get(i).findElement(By.xpath('./td/div/div/div[2]/div/div/div/button')).click()

		WebUI.delay(2)

		WebUI.click(findTestObject('page_advisory_setting/window_delete/btn_cancel'))

		WebUI.delay(1)
		
		doctorButtonTable.get(i).findElement(By.xpath('./td/div/div/div[1]/button')).click()
		
		WebUI.delay(1)
		
		doctorButtonTable.get(i).findElement(By.xpath('./td/div/div/div[2]/div/div/div/button')).click()
		
		WebUI.delay(2)
		
		WebUI.click(findTestObject('page_advisory_setting/window_delete/btn_confirm'))
		
		WebUI.delay(1)
		
		WebUI.verifyElementText(findTestObject('page_advisory_setting/dialog_message'), '刪除成功')
		break
	}
	
	i++
}

WebUI.refresh()

WebUI.delay(2)

//decrease_case_manager//
WebUI.click(findTestObject('page_advisory_setting/tab_case_manager'))

WebUI.delay(1)

case_managerListTable = driver.findElements(By.xpath('//*[@class="patient-info"]/div/div[3]/div[3]/div/div[2]/div/div/div/div/div[1]/div[1]/table/tbody/tr'))
case_managerButtonTable = driver.findElements(By.xpath('//*[@class="patient-info"]/div/div[3]/div[3]/div[2]/div[2]/div/div/div/div/div[2]/div/div/table/tbody/tr'))

i = 0

for (WebElement pointerValue : case_managerListTable) {
	if (pointerValue.getText().contains(case_manager)) {
		case_managerButtonTable.get(i).findElement(By.xpath('./td/div/div/div[1]/button')).click()

		WebUI.delay(1)

		case_managerButtonTable.get(i).findElement(By.xpath('./td/div/div/div[2]/div/div/div/button')).click()

		WebUI.delay(2)

		WebUI.click(findTestObject('page_advisory_setting/window_delete/btn_cancel'))

		WebUI.delay(1)
		
		case_managerButtonTable.get(i).findElement(By.xpath('./td/div/div/div[1]/button')).click()
		
		WebUI.delay(1)
		
		case_managerButtonTable.get(i).findElement(By.xpath('./td/div/div/div[2]/div/div/div/button')).click()
		
		WebUI.delay(2)
		
		WebUI.click(findTestObject('page_advisory_setting/window_delete/btn_confirm'))
		
		WebUI.delay(1)
		
		WebUI.verifyElementText(findTestObject('page_advisory_setting/dialog_message'), '刪除成功')
		break
	}
	
	i++
}

WebUI.refresh()

WebUI.delay(2)

//decrease_consultation_window//
WebUI.click(findTestObject('page_advisory_setting/tab_consultation_window'))

WebUI.delay(1)

consultation_windowListTable = driver.findElements(By.xpath('//*[@class="patient-info"]/div/div[3]/div[4]/div/div[2]/div/div/div/div/div[1]/div[1]/table/tbody/tr'))
consultation_windowButtonTable = driver.findElements(By.xpath('//*[@class="patient-info"]/div/div[3]/div[4]/div[2]/div[2]/div/div/div/div/div[2]/div/div/table/tbody/tr'))

i = 0

for (WebElement pointerValue : consultation_windowListTable) {
	if (pointerValue.getText().contains(consultation_window)) {
		consultation_windowButtonTable.get(i).findElement(By.xpath('./td/div/div/div[1]/button')).click()

		WebUI.delay(1)

		consultation_windowButtonTable.get(i).findElement(By.xpath('./td/div/div/div[2]/div/div/div/button')).click()

		WebUI.delay(2)

		WebUI.click(findTestObject('page_advisory_setting/window_delete/btn_cancel'))

		WebUI.delay(1)
		
		consultation_windowButtonTable.get(i).findElement(By.xpath('./td/div/div/div[1]/button')).click()
		
		WebUI.delay(1)
		
		consultation_windowButtonTable.get(i).findElement(By.xpath('./td/div/div/div[2]/div/div/div/button')).click()
		
		WebUI.delay(2)
		
		WebUI.click(findTestObject('page_advisory_setting/window_delete/btn_confirm'))
		
		WebUI.delay(1)
		
		WebUI.verifyElementText(findTestObject('page_advisory_setting/dialog_message'), '刪除成功')
		break
	}
	
	i++
}

WebUI.refresh()

WebUI.delay(2)

//decrease_wound_specialist//
WebUI.click(findTestObject('page_advisory_setting/tab_wound_specialist'))

WebUI.delay(1)

wound_specialistListTable = driver.findElements(By.xpath('//*[@class="patient-info"]/div/div[3]/div[1]/div/div[2]/div/div/div/div/div[1]/div[1]/table/tbody/tr'))
wound_specialistButtonTable = driver.findElements(By.xpath('//*[@class="patient-info"]/div/div[3]/div[1]/div[2]/div[2]/div/div/div/div/div[2]/div/div/table/tbody/tr'))

i = 0

for (WebElement pointerValue : wound_specialistListTable) {
	if (pointerValue.getText().contains(woundSpecialist)) {
		wound_specialistButtonTable.get(i).findElement(By.xpath('./td/div/div/div[1]/button')).click()

		WebUI.delay(1)

		wound_specialistButtonTable.get(i).findElement(By.xpath('./td/div/div/div[2]/div/div/div/button')).click()

		WebUI.delay(2)

		WebUI.click(findTestObject('page_advisory_setting/window_delete/btn_cancel'))

		WebUI.delay(1)
		
		wound_specialistButtonTable.get(i).findElement(By.xpath('./td/div/div/div[1]/button')).click()
		
		WebUI.delay(1)
		
		wound_specialistButtonTable.get(i).findElement(By.xpath('./td/div/div/div[2]/div/div/div/button')).click()
		
		WebUI.delay(2)
		
		WebUI.click(findTestObject('page_advisory_setting/window_delete/btn_confirm'))
		
		WebUI.delay(1)
		
		WebUI.verifyElementText(findTestObject('page_advisory_setting/dialog_message'), '刪除成功')
		break
	}
	
	i++
}