import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.setText(findTestObject('page_login/input_username'), GlobalVariable.username)

WebUI.setText(findTestObject('page_login/input_password'), GlobalVariable.password)

WebUI.click(findTestObject('page_login/btn_login'))

WebUI.delay(5)

WebUI.click(findTestObject('page_menu/btn_advisory_setting'))

WebUI.delay(1)

WebUI.verifyMatch(WebUI.getUrl(), GlobalVariable.url + 'advisorySetting', false)

WebUI.click(findTestObject('page_advisory_setting/tab_doctor'))

WebUI.delay(1)

WebUI.verifyElementText(findTestObject('page_advisory_setting/page_doctor/title_name'), '姓名')

WebUI.verifyElementText(findTestObject('page_advisory_setting/page_doctor/title_jobTitle'), '職稱')

WebUI.verifyElementText(findTestObject('page_advisory_setting/page_doctor/title_division'), '科別')

WebUI.verifyElementText(findTestObject('page_advisory_setting/page_doctor/title_consultant_name'), '向上諮詢姓名')

WebUI.verifyElementText(findTestObject('page_advisory_setting/page_doctor/title_consultant_jobTitle'), '向上諮詢職稱')

WebUI.verifyElementText(findTestObject('page_advisory_setting/page_doctor/title_consultant_division'), '向上諮詢科別')

WebUI.verifyElementText(findTestObject('page_advisory_setting/page_doctor/title_updateTime'), '更新時間')

WebUI.click(findTestObject('page_advisory_setting/tab_case_manager'))

WebUI.delay(1)

WebUI.verifyElementText(findTestObject('page_advisory_setting/page_case_manager/title_name'), '姓名')

WebUI.verifyElementText(findTestObject('page_advisory_setting/page_case_manager/title_jobTitle'), '職稱')

WebUI.verifyElementText(findTestObject('page_advisory_setting/page_case_manager/title_division'), '科別')

WebUI.verifyElementText(findTestObject('page_advisory_setting/page_case_manager/title_consultant_name'), '向上諮詢姓名')

WebUI.verifyElementText(findTestObject('page_advisory_setting/page_case_manager/title_consultant_jobTitle'), '向上諮詢職稱')

WebUI.verifyElementText(findTestObject('page_advisory_setting/page_case_manager/title_consultant_division'), '向上諮詢科別')

WebUI.verifyElementText(findTestObject('page_advisory_setting/page_case_manager/title_updateTime'), '更新時間')

WebUI.click(findTestObject('page_advisory_setting/tab_consultation_window'))

WebUI.delay(1)

WebUI.verifyElementText(findTestObject('page_advisory_setting/page_consultation_window/title_name'), '姓名')

WebUI.verifyElementText(findTestObject('page_advisory_setting/page_consultation_window/title_jobTitle'), '職稱')

WebUI.verifyElementText(findTestObject('page_advisory_setting/page_consultation_window/title_division'), '科別')

WebUI.verifyElementText(findTestObject('page_advisory_setting/page_consultation_window/title_consultant_name'), '向上諮詢姓名')

WebUI.verifyElementText(findTestObject('page_advisory_setting/page_consultation_window/title_consultant_jobTitle'), '向上諮詢職稱')

WebUI.verifyElementText(findTestObject('page_advisory_setting/page_consultation_window/title_consultant_division'), '向上諮詢科別')

WebUI.verifyElementText(findTestObject('page_advisory_setting/page_consultation_window/title_updateTime'), '更新時間')

WebUI.click(findTestObject('page_advisory_setting/tab_wound_specialist'))

WebUI.delay(1)

WebUI.verifyElementText(findTestObject('page_advisory_setting/page_wound_specialist/title_name'), '姓名')

WebUI.verifyElementText(findTestObject('page_advisory_setting/page_wound_specialist/title_jobTitle'), '職稱')

WebUI.verifyElementText(findTestObject('page_advisory_setting/page_wound_specialist/title_division'), '科別')

WebUI.verifyElementText(findTestObject('page_advisory_setting/page_wound_specialist/title_updateTime'), '更新時間')

