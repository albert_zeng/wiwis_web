import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.By as By
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import org.openqa.selenium.Keys as Keys

WebDriver driver = DriverFactory.getWebDriver()

WebUI.setText(findTestObject('page_login/input_username'), GlobalVariable.username)

WebUI.setText(findTestObject('page_login/input_password'), GlobalVariable.password)

WebUI.click(findTestObject('page_login/btn_login'))

WebUI.delay(2)

WebUI.click(findTestObject('page_menu/btn_long_term_care_management'))

WebUI.delay(1)

WebUI.click(findTestObject('long_term_care_management/long_term_insert_btn'))

WebUI.delay(1)

WebUI.setText(findTestObject('long_term_care_management/long_term_name_input'), 'QTautotest')

WebUI.setText(findTestObject('long_term_care_management/long_term_email_input'), '1@1.com')

WebUI.setText(findTestObject('long_term_care_management/long_term_contact1_input'), 'QTcontact')

WebUI.setText(findTestObject('long_term_care_management/long_term_tel_input'), 'QTtel')

WebUI.delay(1)

WebUI.click(findTestObject('long_term_care_management/long_term_consult_window'))

WebUI.click(findTestObject('long_term_care_management/long_term_consult_window_item'))

WebUI.sendKeys(findTestObject('long_term_care_management/long_term_tel_input'), Keys.chord(Keys.PAGE_DOWN))

WebUI.delay(1)

WebUI.sendKeys(findTestObject('long_term_care_management/long_term_tel_input'), Keys.chord(Keys.PAGE_DOWN))

WebUI.delay(2)

WebUI.click(findTestObject('long_term_care_management/long_term_insert_confirm_btn'))

WebUI.delay(1)

WebUI.setText(findTestObject('page_menu_management/page_dressing_record/dressing/dressing_search_input'), 'QTautotest')

WebUI.sendKeys(findTestObject('page_menu_management/page_dressing_record/dressing/dressing_search_input'), Keys.chord(Keys.ENTER))

WebUI.delay(1)

WebUI.verifyElementText(findTestObject('long_term_care_management/long_term_search_result'), 'QTautotest')

WebUI.delay(1)

WebUI.mouseOver(findTestObject('long_term_care_management/long_term_edit_area'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(1)

WebUI.click(findTestObject('long_term_care_management/long_term_detail_btn'))

WebUI.delay(1)

WebUI.click(findTestObject('page_menu_management/page_dressing_record/dressing/dressing_edit_btn'))

WebUI.delay(1)

WebUI.clearText(findTestObject('long_term_care_management/long_term_name_input'))

WebUI.delay(1)

WebUI.setText(findTestObject('long_term_care_management/long_term_name_input'), 'QTautotestEdit')

WebUI.sendKeys(findTestObject('long_term_care_management/long_term_tel_input'), Keys.chord(Keys.PAGE_DOWN))

WebUI.delay(1)

WebUI.sendKeys(findTestObject('long_term_care_management/long_term_tel_input'), Keys.chord(Keys.PAGE_DOWN))

WebUI.delay(1)

WebUI.click(findTestObject('long_term_care_management/long_term_edit_confirm_btn'))

WebUI.delay(1)

WebUI.click(findTestObject('page_menu_management/page_dressing_record/dressing/btn_dressing_return'))

WebUI.setText(findTestObject('page_menu_management/page_dressing_record/dressing/dressing_search_input'), 'QTautotestEdit')

WebUI.delay(1)

WebUI.sendKeys(findTestObject('page_menu_management/page_dressing_record/dressing/dressing_search_input'), Keys.chord(Keys.ENTER))

WebUI.delay(1)

WebUI.mouseOver(findTestObject('long_term_care_management/long_term_edit_area'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(1)

WebUI.click(findTestObject('long_term_care_management/long_term_delete_btn'))

WebUI.delay(1)

WebUI.click(findTestObject('long_term_care_management/long_term_delete_confirm_btn'))

WebUI.delay(1)

WebUI.verifyElementText(findTestObject('page_menu_management/page_dressing_record/dressing/dressing_delete_msg'), '刪除成功')

