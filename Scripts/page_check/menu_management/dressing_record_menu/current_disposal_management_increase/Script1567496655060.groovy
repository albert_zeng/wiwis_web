import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.By as By
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement

WebDriver driver = DriverFactory.getWebDriver()

WebUI.setText(findTestObject('page_login/input_username'), GlobalVariable.username)

WebUI.setText(findTestObject('page_login/input_password'), GlobalVariable.password)

WebUI.click(findTestObject('page_login/btn_login'))

WebUI.delay(5)

WebUI.click(findTestObject('page_menu/btn_menu_management'))

WebUI.delay(1)

WebUI.click(findTestObject('page_menu/btn_menu_management_dressingRecord'))

WebUI.delay(1)

WebUI.click(findTestObject('page_menu_management/page_dressing_record/tab_current_disposal'))

WebUI.delay(1)

WebUI.click(findTestObject('page_menu_management/page_dressing_record/current_disposal/btn_increase_current_disposal'))

WebUI.delay(1)

WebUI.verifyElementText(findTestObject('page_menu_management/page_dressing_record/window_increase/text_increase_label'), 
    '現行處置名稱')

WebUI.verifyElementAttributeValue(findTestObject('page_menu_management/page_dressing_record/current_disposal/input_increase_current_disposal'), 
    'placeholder', '輸入現行處置名稱', 0)

WebUI.setText(findTestObject('page_menu_management/page_dressing_record/current_disposal/input_increase_current_disposal'), 
    current_disposal)

WebUI.click(findTestObject('page_menu_management/page_dressing_record/window_increase/btn_increase_confirm'))

WebUI.delay(1)

WebUI.setText(findTestObject('page_menu_management/page_dressing_record/current_disposal/input_current_disposal_search'), 
    current_disposal)

WebUI.click(findTestObject('page_menu_management/page_dressing_record/current_disposal/btn_current_disposal_search'))

WebUI.delay(1)

List<WebElement> current_disposalListTable = driver.findElements(By.xpath('//*[@id="root"]/section/section/main/div/div/div[3]/div[4]/div[2]/div[2]/div/div/div/div/div/div/div/div[1]/div/table/tbody/tr'))

List<WebElement> current_disposalButtonTable = driver.findElements(By.xpath('//*[@id="root"]/section/section/main/div/div/div[3]/div[4]/div[2]/div[2]/div/div/div/div/div/div/div/div[2]/div/div/table/tbody/tr'))

i = 0

for (WebElement pointerValue : current_disposalListTable) {
    if (pointerValue.getText().equals(current_disposal)) {
        current_disposalButtonTable.get(i).findElement(By.xpath('./td/div/div/div[1]/button')).click()

        WebUI.delay(1)

        current_disposalButtonTable.get(i).findElement(By.xpath('./td/div/div/div[2]/div/div/div/button[2]')).click()

        WebUI.delay(1)

        WebUI.click(findTestObject('page_menu_management/page_dressing_record/window_delete/btn_confirm'))

        break
    }
    
    i++
}

WebUI.delay(1)

