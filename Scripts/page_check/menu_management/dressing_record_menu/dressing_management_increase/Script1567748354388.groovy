import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.By as By
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import org.openqa.selenium.Keys as Keys

WebDriver driver = DriverFactory.getWebDriver()

WebUI.setText(findTestObject('page_login/input_username'), GlobalVariable.username)

WebUI.setText(findTestObject('page_login/input_password'), GlobalVariable.password)

WebUI.click(findTestObject('page_login/btn_login'))

WebUI.delay(2)

WebUI.click(findTestObject('page_menu/btn_menu_management'))

WebUI.delay(1)

WebUI.click(findTestObject('page_menu/btn_menu_management_dressingRecord'))

WebUI.delay(1)

WebUI.click(findTestObject('page_menu_management/page_dressing_record/tab_dressing'))

WebUI.delay(1)

WebUI.click(findTestObject('page_menu_management/page_dressing_record/dressing_insert_btn'))

WebUI.delay(1)

WebUI.setText(findTestObject('page_menu_management/page_dressing_record/dressing/dressing_name_input'), '老協珍')

WebUI.delay(1)

WebUI.uploadFile(findTestObject('page_menu_management/page_dressing_record/dressing/dressing_upload_input'), 'C:\\2.jpg')

WebUI.delay(3)

WebUI.click(findTestObject('page_menu_management/page_dressing_record/dressing/dressing_confirm_btn'))

WebUI.delay(1)

WebUI.setText(findTestObject('page_menu_management/page_dressing_record/dressing/dressing_search_input'), '老協珍')

WebUI.sendKeys(findTestObject('page_menu_management/page_dressing_record/dressing/dressing_search_input'), Keys.chord(Keys.ENTER))

WebUI.delay(1)

WebUI.verifyElementText(findTestObject('page_menu_management/page_dressing_record/dressing/dressing_search_result'), '老協珍')

WebUI.delay(1)

WebUI.mouseOver(findTestObject('page_menu_management/page_dressing_record/dressing/dressing_edit_area'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(1)

WebUI.click(findTestObject('page_menu_management/page_dressing_record/dressing/dressing_detail_btn'))

WebUI.delay(1)

WebUI.click(findTestObject('page_menu_management/page_dressing_record/dressing/dressing_edit_btn'))

WebUI.delay(1)

WebUI.clearText(findTestObject('page_menu_management/page_dressing_record/dressing/input_edit_dressing_name'))

WebUI.delay(1)

WebUI.setText(findTestObject('page_menu_management/page_dressing_record/dressing/input_edit_dressing_name'), '真人生')

WebUI.click(findTestObject('page_menu_management/page_dressing_record/dressing/dressing_edit_confirm_btn'))

WebUI.delay(1)

WebUI.click(findTestObject('page_menu_management/page_dressing_record/dressing/btn_dressing_return'))

WebUI.setText(findTestObject('page_menu_management/page_dressing_record/dressing/dressing_search_input'), '真人生')

WebUI.delay(1)

WebUI.sendKeys(findTestObject('page_menu_management/page_dressing_record/dressing/dressing_search_input'), Keys.chord(Keys.ENTER))

WebUI.delay(1)

WebUI.mouseOver(findTestObject('page_menu_management/page_dressing_record/dressing/dressing_edit_area'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(1)

WebUI.click(findTestObject('page_menu_management/page_dressing_record/dressing/btn_dressing_delete'))

WebUI.delay(1)

WebUI.click(findTestObject('page_menu_management/page_dressing_record/dressing/btn_dressing_delete_confirm'))

WebUI.delay(2)

WebUI.verifyElementText(findTestObject('page_menu_management/page_dressing_record/dressing/dressing_delete_msg'), '刪除成功')

