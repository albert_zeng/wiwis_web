import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.By as By
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import java.awt.Robot as Robot
import java.awt.event.InputEvent as InputEvent
import java.awt.MouseInfo as MouseInfo

WebDriver driver = DriverFactory.getWebDriver()

Robot robot = new Robot()

WebUI.setText(findTestObject('page_login/input_username'), GlobalVariable.username)

WebUI.setText(findTestObject('page_login/input_password'), GlobalVariable.password)

WebUI.click(findTestObject('page_login/btn_login'))

WebUI.delay(2)

WebUI.click(findTestObject('page_menu/btn_menu_management'))

WebUI.delay(1)

WebUI.click(findTestObject('page_menu/btn_menu_management_dressingRecord'))

WebUI.delay(1)

WebUI.click(findTestObject('page_menu_management/page_dressing_record/tab_dressing'))

WebUI.delay(1)

List<WebElement> DisceaseHistoryListTable = driver.findElements(By.xpath('//*[@id="root"]/section/section/main/div/div/div[3]/div[2]/div[2]/div[2]/div/div/div/div/div/div/div/div[1]/div/table/tbody/tr'))
 
 robot.mouseMove(0, 0)
 
 robot.mouseMove(100, 100)
 
 zoom = (MouseInfo.getPointerInfo().getLocation().getX() / 100)
 
 origin_x = (((DisceaseHistoryListTable.get(sortFromNum).getLocation().getX() / zoom) + (DisceaseHistoryListTable.get(sortFromNum).getSize().getWidth() /
	 3)) as int)
 
 origin_y = (((DisceaseHistoryListTable.get(sortFromNum).getLocation().getY() / zoom) + (DisceaseHistoryListTable.get(sortFromNum).getSize().getHeight() *
	 2.4)) as int)
 
 destination_x = (((DisceaseHistoryListTable.get(sortToNum).getLocation().getX() / zoom) + (DisceaseHistoryListTable.get(
		 sortToNum).getSize().getWidth() / 3)) as int)
 
 destination_y = (((DisceaseHistoryListTable.get(sortToNum).getLocation().getY() / zoom) + (DisceaseHistoryListTable.get(
		 sortToNum).getSize().getHeight() * 2.4)) as int)
 
 robot.mouseMove(0, 0)
 
 WebUI.delay(1)
 
 robot.mouseMove(origin_x, origin_y)
 
 robot.mousePress(InputEvent.BUTTON1_MASK)
 
 robot.mouseMove(0, 0)
 
 WebUI.delay(1)
 
 robot.mouseMove(destination_x, destination_y)
 
 robot.mouseRelease(InputEvent.BUTTON1_MASK)
 
 WebUI.verifyElementText(findTestObject('menu_management/switch_success_message'), '成功更新順序')
 
 WebUI.delay(2)
 

