import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.setText(findTestObject('page_login/input_username'), GlobalVariable.username)

WebUI.setText(findTestObject('page_login/input_password'), GlobalVariable.password)

WebUI.click(findTestObject('page_login/btn_login'))

WebUI.delay(5)

WebUI.click(findTestObject('page_menu/btn_menu_management'))

WebUI.delay(1)

WebUI.click(findTestObject('page_menu/btn_menu_management_patientInfo'))

WebUI.verifyMatch(WebUI.getUrl(), GlobalVariable.url + 'patientInfo', false)

WebUI.verifyElementText(findTestObject('page_menu_management/page_patient_info/title_personal_history'), '個人史名稱')

WebUI.click(findTestObject('page_menu_management/page_patient_info/tab_medical_history'))

WebUI.delay(1)

WebUI.verifyElementText(findTestObject('page_menu_management/page_patient_info/title_medical_history'), '疾病史名稱')

WebUI.click(findTestObject('page_menu_management/page_patient_info/tab_personal_history'))

WebUI.delay(1)

WebUI.verifyElementText(findTestObject('page_menu_management/page_patient_info/title_personal_history'), '個人史名稱')

WebUI.click(findTestObject('page_menu/btn_menu_management_dressingRecord'))

WebUI.verifyMatch(WebUI.getUrl(), GlobalVariable.url + 'dressingRecord', false)

WebUI.verifyElementText(findTestObject('page_menu_management/page_dressing_record/medicine/title_medicine'), '藥膏名稱')

WebUI.click(findTestObject('page_menu_management/page_dressing_record/tab_dressing'))

WebUI.delay(1)

WebUI.verifyElementText(findTestObject('page_menu_management/page_dressing_record/dressing/title_dressing_picture'), '圖片')

WebUI.verifyElementText(findTestObject('page_menu_management/page_dressing_record/dressing/title_dressing_name'), '敷料名稱')

WebUI.verifyElementText(findTestObject('page_menu_management/page_dressing_record/dressing/title_dressing_introduction'), 
    '敷料介紹')

WebUI.verifyElementText(findTestObject('page_menu_management/page_dressing_record/dressing/title_dressing_instructions'), 
    '適用時機與優點')

WebUI.verifyElementText(findTestObject('page_menu_management/page_dressing_record/dressing/title_dressing_price'), '價格(元)')

WebUI.click(findTestObject('page_menu_management/page_dressing_record/tab_special_material'))

WebUI.delay(1)

WebUI.verifyElementText(findTestObject('page_menu_management/page_dressing_record/special_material/title_special_meterial'), 
    '特殊器材名稱')

WebUI.click(findTestObject('page_menu_management/page_dressing_record/tab_current_disposal'))

WebUI.delay(1)

WebUI.verifyElementText(findTestObject('page_menu_management/page_dressing_record/current_disposal/title_current_disposal'), 
    '現行處置名稱')

WebUI.click(findTestObject('page_menu_management/page_dressing_record/tab_medicine'))

WebUI.delay(1)

WebUI.verifyElementText(findTestObject('page_menu_management/page_dressing_record/medicine/title_medicine'), '藥膏名稱')

