import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.setText(findTestObject('page_login/input_username'), GlobalVariable.username)

WebUI.setText(findTestObject('page_login/input_password'), GlobalVariable.password)

WebUI.click(findTestObject('page_login/btn_login'))

WebUI.delay(2)

WebUI.click(findTestObject('menu_management/menu_management'))

WebUI.delay(1)

WebUI.click(findTestObject('menu_management/patient_information_menu'))

WebUI.delay(1)

WebUI.click(findTestObject('page_menu_management/page_patient_info/tab_medical_history'))

WebUI.delay(1)

WebUI.click(findTestObject('menu_management/insert_btn_discease_history'))

WebUI.delay(1)

WebUI.setText(findTestObject('menu_management/discease_histrory_input'), '相思病')

WebUI.delay(1)

WebUI.click(findTestObject('menu_management/edit_confirm_btn'))

WebUI.delay(1)

WebUI.setText(findTestObject('menu_management/discease_search_input'), '相思病')

WebUI.delay(1)

WebUI.click(findTestObject('menu_management/discease_search_btn'))

WebUI.delay(1)

WebUI.verifyElementText(findTestObject('menu_management/discease_search_result'), '相思病')

WebUI.delay(1)

WebUI.mouseOver(findTestObject('menu_management/discease_edit_area'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(1)

WebUI.click(findTestObject('menu_management/discease_edit_btn'))

WebUI.delay(1)

WebUI.clearText(findTestObject('menu_management/discease_histrory_input'))

WebUI.delay(1)

WebUI.setText(findTestObject('menu_management/discease_histrory_input'), '小感冒')

WebUI.delay(1)

WebUI.click(findTestObject('menu_management/edit_confirm_btn'))

WebUI.clearText(findTestObject('menu_management/discease_search_input'))

WebUI.delay(1)

WebUI.setText(findTestObject('menu_management/discease_search_input'), '小感冒')

WebUI.delay(1)

WebUI.click(findTestObject('menu_management/discease_search_btn'))

WebUI.delay(1)

WebUI.mouseOver(findTestObject('menu_management/discease_edit_area'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(1)

WebUI.click(findTestObject('menu_management/discease_delete_btn'))

WebUI.delay(1)

WebUI.click(findTestObject('menu_management/delete_confirm_btn'))

WebUI.delay(1)

