import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.InternalData as InternalData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.By as By
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement

WebDriver driver = DriverFactory.getWebDriver()

InternalData searchData = findTestData('Data Files/exceptionSearchData')

WebUI.setText(findTestObject('page_login/input_username'), GlobalVariable.username)

WebUI.setText(findTestObject('page_login/input_password'), GlobalVariable.password)

WebUI.click(findTestObject('page_login/btn_login'))

WebUI.delay(5)

for (int i = 0; i < searchData.getRowNumbers(); i++) {
    WebUI.setText(findTestObject('page_patient_management/input_search'), searchData.internallyGetValue('searchValue', i))

    WebUI.click(findTestObject('page_patient_management/btn_search'))

    WebUI.delay(2)

    List<String> patientListTable = driver.findElements(By.xpath('//*[@id="root"]/section/section/main/div/div/div/div/div/div/div/div/div[1]/table/tbody/tr'))

    for (WebElement pointerValue : patientListTable) {
        value = 'false'

        if (pointerValue.findElement(By.xpath('.//td[2]')).getText().contains(searchData.internallyGetValue('searchValue', 
                i))) {
            value = 'true'
        } else {
            value = 'false'
        }
        
        println(searchData.internallyGetValue('searchValue', i) + value)

        WebUI.verifyMatch(value, 'true', false)
    }
}

