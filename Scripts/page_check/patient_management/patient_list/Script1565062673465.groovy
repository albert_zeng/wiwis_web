import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.setText(findTestObject('page_login/input_username'), GlobalVariable.username)

WebUI.setText(findTestObject('page_login/input_password'), GlobalVariable.password)

WebUI.click(findTestObject('page_login/btn_login'))

WebUI.delay(5)

WebUI.verifyElementAttributeValue(findTestObject('page_patient_management/span_checkbox_hospitalization'), 'class', 'ant-checkbox ant-checkbox-checked', 
    0)

WebUI.verifyElementAttributeValue(findTestObject('page_patient_management/span_checkbox_daypass'), 'class', 'ant-checkbox ant-checkbox-checked', 
    0)

WebUI.verifyElementAttributeValue(findTestObject('page_patient_management/span_checkbox_care'), 'class', 'ant-checkbox ant-checkbox-checked', 
    0)

WebUI.verifyElementAttributeValue(findTestObject('page_patient_management/span_checkbox_discharged'), 'class', 'ant-checkbox ant-checkbox-checked', 
    0)

WebUI.verifyElementAttributeValue(findTestObject('page_patient_management/span_checkbox_unknown'), 'class', 'ant-checkbox ant-checkbox-checked', 
    0)

WebUI.verifyElementAttributeValue(findTestObject('page_patient_management/input_search'), 'placeholder', '輸入關鍵字', 0)

WebUI.verifyElementText(findTestObject('page_patient_management/title_patientUnit'), '單位／組織名稱')

WebUI.verifyElementText(findTestObject('page_patient_management/title_paientName'), '病患姓名')

WebUI.verifyElementText(findTestObject('page_patient_management/title_patientId'), '身分證字號')

WebUI.verifyElementText(findTestObject('page_patient_management/title_patientGender'), '性別')

WebUI.verifyElementText(findTestObject('page_patient_management/title_patientBirthday'), '生日')

WebUI.verifyElementText(findTestObject('page_patient_management/title_patientPhone'), '電話')

WebUI.verifyElementText(findTestObject('page_patient_management/title_patientBedNumber'), '床號')

WebUI.verifyElementText(findTestObject('page_patient_management/title_attendingPhysician'), '主治醫師')

WebUI.verifyElementText(findTestObject('page_patient_management/title_patientStatus'), '病患狀態')

