import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.setText(findTestObject('page_login/input_username'), GlobalVariable.username)

WebUI.setText(findTestObject('page_login/input_password'), GlobalVariable.password)

WebUI.click(findTestObject('page_login/btn_login'))

WebUI.delay(5)

WebUI.click(findTestObject('page_patient_management/checkbox_daypass'))

WebUI.click(findTestObject('page_patient_management/checkbox_homecare'))

WebUI.click(findTestObject('page_patient_management/checkbox_out_hospital'))

WebUI.click(findTestObject('page_patient_management/checkbox_unknown'))

WebUI.delay(1)

WebUI.verifyElementText(findTestObject('page_patient_management/text_patient_status'), '住院')

WebUI.delay(1)

WebUI.click(findTestObject('page_patient_management/checkbox_daypass'))

WebUI.click(findTestObject('page_patient_management/checkbox_in_hospital'))

WebUI.delay(2)

WebUI.verifyElementText(findTestObject('page_patient_management/text_patient_status'), 'DayPass')

WebUI.click(findTestObject('page_patient_management/checkbox_homecare'))

WebUI.click(findTestObject('page_patient_management/checkbox_daypass'))

WebUI.delay(2)

WebUI.verifyElementText(findTestObject('page_patient_management/text_patient_status'), '居家照護')

WebUI.click(findTestObject('page_patient_management/checkbox_out_hospital'))

WebUI.click(findTestObject('page_patient_management/checkbox_homecare'))

WebUI.delay(2)

WebUI.verifyElementText(findTestObject('page_patient_management/text_patient_status'), '出院')

WebUI.delay(2)

WebUI.click(findTestObject('page_patient_management/checkbox_unknown'))

WebUI.click(findTestObject('page_patient_management/checkbox_out_hospital'))

WebUI.delay(2)

WebUI.verifyElementText(findTestObject('page_patient_management/text_patient_status'), '- -')

