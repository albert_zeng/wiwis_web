import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.By as By
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement

WebDriver driver = DriverFactory.getWebDriver()

WebUI.setText(findTestObject('page_login/input_username'), GlobalVariable.username)

WebUI.setText(findTestObject('page_login/input_password'), GlobalVariable.password)

WebUI.click(findTestObject('page_login/btn_login'))

WebUI.delay(5)

List<WebElement> patientListTable = driver.findElements(By.xpath('//*[@id="root"]/section/section/main/div/div/div/div/div/div/div/div/div[1]/div[1]/table/tbody/tr'))

List<WebElement> pagingTable = driver.findElements(By.xpath('//*[@id="root"]/section/section/main/div/div/div/div/div/div/ul/li'))

pageInfo = WebUI.getText(findTestObject('page_patient_management/text_page_info'))

i = 0

j = patientListTable.size()

String[] pageInfo_split = new String[3]

for (String retval: pageInfo.split("/| ")){
	pageInfo_split[i++] = retval
}

WebUI.verifyMatch(patientListTable.size().toString(), pageInfo_split[0], false)

WebUI.verifyMatch(pagingTable[0].getAttribute("aria-disabled"), "true", false)

if ((pageInfo_split[1] as int) < 100) {

	if ((pageInfo_split[1] as int) <= 10){
		WebUI.verifyMatch(pagingTable[2].getAttribute("aria-disabled"), "true", false)
		
	} else {
		for (i = 2;i < pagingTable.size()-1;i++){
			pagingTable[i].click()

			WebUI.delay(2)

			patientListTable = driver.findElements(By.xpath('//*[@id="root"]/section/section/main/div/div/div/div/div/div/div/div/div[1]/div[1]/table/tbody/tr'))
			
			j = j + patientListTable.size()
			
			if (i == pagingTable.size()-2) {
				WebUI.verifyMatch(j.toString(), pageInfo_split[1], false)
				
				WebUI.verifyMatch(pagingTable[i+1].getAttribute("aria-disabled"), "true", false)
			}
		}
	}
}

