import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.By as By
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import java.util.List as List
import java.util.ArrayList as ArrayList
import java.util.Locale as Locale
import java.util.Collections as Collections
import java.text.Collator as Collator

WebDriver driver = DriverFactory.getWebDriver()

List<WebElement> testSort2 = new ArrayList<String>()

testSort2.add('王中平')

testSort2.add('陳一陳')

testSort2.add('李中原')

testSort2.add('趙天成')

testSort2.add('大中華')

testSort2.add('林木森')

testSort2.add('王一陳')

testSort2.add('李中陳')

testSort2.add('李天成')

testSort2.add('大中原')

Collator collatorChinese = Collator.getInstance(java.util.Locale.TAIWAN)

Collections.sort(testSort2, collatorChinese)

for (int i = 0; i < testSort2.size(); i++) {
    String chinese = testSort2.get(i)

    println('默認中文字串筆劃排序： = ' + chinese)
}

