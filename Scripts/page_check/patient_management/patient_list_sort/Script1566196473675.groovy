import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.By as By
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement

WebDriver driver = DriverFactory.getWebDriver()

WebUI.setText(findTestObject('page_login/input_username'), GlobalVariable.username)

WebUI.setText(findTestObject('page_login/input_password'), GlobalVariable.password)

WebUI.click(findTestObject('page_login/btn_login'))

WebUI.delay(5)

List<WebElement> patientListTable = driver.findElements(By.xpath('//*[@id="root"]/section/section/main/div/div/div/div/div/div/div/div/div[1]/table/tbody/tr'))

WebUI.click(findTestObject('page_patient_management/title_paientName'))

WebUI.delay(1)

WebElement patient_sort_up = driver.findElement(By.xpath('//*[@id="root"]/section/section/main/div/div/div/div/div/div/div/div/div[1]/table/thead/tr/th[2]/span/div/span[2]/div/i[1]'))

WebUI.verifyMatch(patient_sort_up.getAttribute('class').substring(52), 'on', false)

WebElement patient_sort_down = driver.findElement(By.xpath('//*[@id="root"]/section/section/main/div/div/div/div/div/div/div/div/div[1]/table/thead/tr/th[2]/span/div/span[2]/div/i[2]'))

WebUI.verifyMatch(patient_sort_down.getAttribute('class').substring(56), 'off', false)

for (WebElement pointerValue : patientListTable) {
    WebUI.verifyMatch(pointerValue.findElement(By.xpath('.//td[2]')).getAttribute('class').substring(58), 'ant-table-column-sort', 
        false)
}

WebUI.click(findTestObject('page_patient_management/title_paientName'))

WebUI.delay(1)

patient_sort_up = driver.findElement(By.xpath('//*[@id="root"]/section/section/main/div/div/div/div/div/div/div/div/div[1]/table/thead/tr/th[2]/span/div/span[2]/div/i[1]'))

WebUI.verifyMatch(patient_sort_up.getAttribute('class').substring(52), 'off', false)

patient_sort_down = driver.findElement(By.xpath('//*[@id="root"]/section/section/main/div/div/div/div/div/div/div/div/div[1]/table/thead/tr/th[2]/span/div/span[2]/div/i[2]'))

WebUI.verifyMatch(patient_sort_down.getAttribute('class').substring(56), 'on', false)

for (WebElement pointerValue : patientListTable) {
    WebUI.verifyMatch(pointerValue.findElement(By.xpath('.//td[2]')).getAttribute('class').substring(58), 'ant-table-column-sort', 
        false)
}

WebUI.click(findTestObject('page_patient_management/title_patientId'))

WebUI.delay(1)

patient_sort_up = driver.findElement(By.xpath('//*[@id="root"]/section/section/main/div/div/div/div/div/div/div/div/div[1]/table/thead/tr/th[3]/span/div/span[2]/div/i[1]'))

WebUI.verifyMatch(patient_sort_up.getAttribute('class').substring(52), 'on', false)

patient_sort_down = driver.findElement(By.xpath('//*[@id="root"]/section/section/main/div/div/div/div/div/div/div/div/div[1]/table/thead/tr/th[3]/span/div/span[2]/div/i[2]'))

WebUI.verifyMatch(patient_sort_down.getAttribute('class').substring(56), 'off', false)

for (WebElement pointerValue : patientListTable) {
    WebUI.verifyMatch(pointerValue.findElement(By.xpath('.//td[3]')).getAttribute('class').substring(58), 'ant-table-column-sort', 
        false)
}

WebUI.click(findTestObject('page_patient_management/title_patientId'))

WebUI.delay(1)

patient_sort_up = driver.findElement(By.xpath('//*[@id="root"]/section/section/main/div/div/div/div/div/div/div/div/div[1]/table/thead/tr/th[3]/span/div/span[2]/div/i[1]'))

WebUI.verifyMatch(patient_sort_up.getAttribute('class').substring(52), 'off', false)

patient_sort_down = driver.findElement(By.xpath('//*[@id="root"]/section/section/main/div/div/div/div/div/div/div/div/div[1]/table/thead/tr/th[3]/span/div/span[2]/div/i[2]'))

WebUI.verifyMatch(patient_sort_down.getAttribute('class').substring(56), 'on', false)

for (WebElement pointerValue : patientListTable) {
    WebUI.verifyMatch(pointerValue.findElement(By.xpath('.//td[3]')).getAttribute('class').substring(58), 'ant-table-column-sort', 
        false)
}

WebUI.click(findTestObject('page_patient_management/title_patientGender'))

WebUI.delay(1)

patient_sort_up = driver.findElement(By.xpath('//*[@id="root"]/section/section/main/div/div/div/div/div/div/div/div/div[1]/table/thead/tr/th[4]/span/div/span[2]/div/i[1]'))

WebUI.verifyMatch(patient_sort_up.getAttribute('class').substring(52), 'on', false)

patient_sort_down = driver.findElement(By.xpath('//*[@id="root"]/section/section/main/div/div/div/div/div/div/div/div/div[1]/table/thead/tr/th[4]/span/div/span[2]/div/i[2]'))

WebUI.verifyMatch(patient_sort_down.getAttribute('class').substring(56), 'off', false)

for (WebElement pointerValue : patientListTable) {
    WebUI.verifyMatch(pointerValue.findElement(By.xpath('.//td[4]')).getAttribute('class').substring(58), 'ant-table-column-sort', 
        false)
}

WebUI.click(findTestObject('page_patient_management/title_patientGender'))

WebUI.delay(1)

patient_sort_up = driver.findElement(By.xpath('//*[@id="root"]/section/section/main/div/div/div/div/div/div/div/div/div[1]/table/thead/tr/th[4]/span/div/span[2]/div/i[1]'))

WebUI.verifyMatch(patient_sort_up.getAttribute('class').substring(52), 'off', false)

patient_sort_down = driver.findElement(By.xpath('//*[@id="root"]/section/section/main/div/div/div/div/div/div/div/div/div[1]/table/thead/tr/th[4]/span/div/span[2]/div/i[2]'))

WebUI.verifyMatch(patient_sort_down.getAttribute('class').substring(56), 'on', false)

for (WebElement pointerValue : patientListTable) {
    WebUI.verifyMatch(pointerValue.findElement(By.xpath('.//td[4]')).getAttribute('class').substring(58), 'ant-table-column-sort', 
        false)
}

WebUI.click(findTestObject('page_patient_management/title_patientBirthday'))

WebUI.delay(1)

patient_sort_up = driver.findElement(By.xpath('//*[@id="root"]/section/section/main/div/div/div/div/div/div/div/div/div[1]/table/thead/tr/th[5]/span/div/span[2]/div/i[1]'))

WebUI.verifyMatch(patient_sort_up.getAttribute('class').substring(52), 'on', false)

patient_sort_down = driver.findElement(By.xpath('//*[@id="root"]/section/section/main/div/div/div/div/div/div/div/div/div[1]/table/thead/tr/th[5]/span/div/span[2]/div/i[2]'))

WebUI.verifyMatch(patient_sort_down.getAttribute('class').substring(56), 'off', false)

for (WebElement pointerValue : patientListTable) {
    WebUI.verifyMatch(pointerValue.findElement(By.xpath('.//td[5]')).getAttribute('class').substring(58), 'ant-table-column-sort', 
        false)
}

WebUI.click(findTestObject('page_patient_management/title_patientBirthday'))

WebUI.delay(1)

patient_sort_up = driver.findElement(By.xpath('//*[@id="root"]/section/section/main/div/div/div/div/div/div/div/div/div[1]/table/thead/tr/th[5]/span/div/span[2]/div/i[1]'))

WebUI.verifyMatch(patient_sort_up.getAttribute('class').substring(52), 'off', false)

patient_sort_down = driver.findElement(By.xpath('//*[@id="root"]/section/section/main/div/div/div/div/div/div/div/div/div[1]/table/thead/tr/th[5]/span/div/span[2]/div/i[2]'))

WebUI.verifyMatch(patient_sort_down.getAttribute('class').substring(56), 'on', false)

for (WebElement pointerValue : patientListTable) {
    WebUI.verifyMatch(pointerValue.findElement(By.xpath('.//td[5]')).getAttribute('class').substring(58), 'ant-table-column-sort', 
        false)
}

WebUI.click(findTestObject('page_patient_management/title_patientPhone'))

WebUI.delay(1)

patient_sort_up = driver.findElement(By.xpath('//*[@id="root"]/section/section/main/div/div/div/div/div/div/div/div/div[1]/table/thead/tr/th[6]/span/div/span[2]/div/i[1]'))

WebUI.verifyMatch(patient_sort_up.getAttribute('class').substring(52), 'on', false)

patient_sort_down = driver.findElement(By.xpath('//*[@id="root"]/section/section/main/div/div/div/div/div/div/div/div/div[1]/table/thead/tr/th[6]/span/div/span[2]/div/i[2]'))

WebUI.verifyMatch(patient_sort_down.getAttribute('class').substring(56), 'off', false)

for (WebElement pointerValue : patientListTable) {
    WebUI.verifyMatch(pointerValue.findElement(By.xpath('.//td[6]')).getAttribute('class').substring(58), 'ant-table-column-sort', 
        false)
}

WebUI.click(findTestObject('page_patient_management/title_patientPhone'))

WebUI.delay(1)

patient_sort_up = driver.findElement(By.xpath('//*[@id="root"]/section/section/main/div/div/div/div/div/div/div/div/div[1]/table/thead/tr/th[6]/span/div/span[2]/div/i[1]'))

WebUI.verifyMatch(patient_sort_up.getAttribute('class').substring(52), 'off', false)

patient_sort_down = driver.findElement(By.xpath('//*[@id="root"]/section/section/main/div/div/div/div/div/div/div/div/div[1]/table/thead/tr/th[6]/span/div/span[2]/div/i[2]'))

WebUI.verifyMatch(patient_sort_down.getAttribute('class').substring(56), 'on', false)

for (WebElement pointerValue : patientListTable) {
    WebUI.verifyMatch(pointerValue.findElement(By.xpath('.//td[6]')).getAttribute('class').substring(58), 'ant-table-column-sort', 
        false)
}

WebUI.click(findTestObject('page_patient_management/title_patientBedNumber'))

WebUI.delay(1)

patient_sort_up = driver.findElement(By.xpath('//*[@id="root"]/section/section/main/div/div/div/div/div/div/div/div/div[1]/table/thead/tr/th[7]/span/div/span[2]/div/i[1]'))

WebUI.verifyMatch(patient_sort_up.getAttribute('class').substring(52), 'on', false)

patient_sort_down = driver.findElement(By.xpath('//*[@id="root"]/section/section/main/div/div/div/div/div/div/div/div/div[1]/table/thead/tr/th[7]/span/div/span[2]/div/i[2]'))

WebUI.verifyMatch(patient_sort_down.getAttribute('class').substring(56), 'off', false)

for (WebElement pointerValue : patientListTable) {
    WebUI.verifyMatch(pointerValue.findElement(By.xpath('.//td[7]')).getAttribute('class').substring(58), 'ant-table-column-sort', 
        false)
}

WebUI.click(findTestObject('page_patient_management/title_patientBedNumber'))

WebUI.delay(1)

patient_sort_up = driver.findElement(By.xpath('//*[@id="root"]/section/section/main/div/div/div/div/div/div/div/div/div[1]/table/thead/tr/th[7]/span/div/span[2]/div/i[1]'))

WebUI.verifyMatch(patient_sort_up.getAttribute('class').substring(52), 'off', false)

patient_sort_down = driver.findElement(By.xpath('//*[@id="root"]/section/section/main/div/div/div/div/div/div/div/div/div[1]/table/thead/tr/th[7]/span/div/span[2]/div/i[2]'))

WebUI.verifyMatch(patient_sort_down.getAttribute('class').substring(56), 'on', false)

for (WebElement pointerValue : patientListTable) {
    WebUI.verifyMatch(pointerValue.findElement(By.xpath('.//td[7]')).getAttribute('class').substring(58), 'ant-table-column-sort', 
        false)
}

WebUI.click(findTestObject('page_patient_management/title_attendingPhysician'))

WebUI.delay(1)

patient_sort_up = driver.findElement(By.xpath('//*[@id="root"]/section/section/main/div/div/div/div/div/div/div/div/div[1]/table/thead/tr/th[8]/span/div/span[2]/div/i[1]'))

WebUI.verifyMatch(patient_sort_up.getAttribute('class').substring(52), 'on', false)

patient_sort_down = driver.findElement(By.xpath('//*[@id="root"]/section/section/main/div/div/div/div/div/div/div/div/div[1]/table/thead/tr/th[8]/span/div/span[2]/div/i[2]'))

WebUI.verifyMatch(patient_sort_down.getAttribute('class').substring(56), 'off', false)

for (WebElement pointerValue : patientListTable) {
    WebUI.verifyMatch(pointerValue.findElement(By.xpath('.//td[8]')).getAttribute('class').substring(58), 'ant-table-column-sort', 
        false)
}

WebUI.click(findTestObject('page_patient_management/title_attendingPhysician'))

WebUI.delay(1)

patient_sort_up = driver.findElement(By.xpath('//*[@id="root"]/section/section/main/div/div/div/div/div/div/div/div/div[1]/table/thead/tr/th[8]/span/div/span[2]/div/i[1]'))

WebUI.verifyMatch(patient_sort_up.getAttribute('class').substring(52), 'off', false)

patient_sort_down = driver.findElement(By.xpath('//*[@id="root"]/section/section/main/div/div/div/div/div/div/div/div/div[1]/table/thead/tr/th[8]/span/div/span[2]/div/i[2]'))

WebUI.verifyMatch(patient_sort_down.getAttribute('class').substring(56), 'on', false)

for (WebElement pointerValue : patientListTable) {
    WebUI.verifyMatch(pointerValue.findElement(By.xpath('.//td[8]')).getAttribute('class').substring(58), 'ant-table-column-sort', 
        false)
}

WebUI.click(findTestObject('page_patient_management/title_patientStatus'))

WebUI.delay(1)

patient_sort_up = driver.findElement(By.xpath('//*[@id="root"]/section/section/main/div/div/div/div/div/div/div/div/div[1]/table/thead/tr/th[9]/span/div/span[2]/div/i[1]'))

WebUI.verifyMatch(patient_sort_up.getAttribute('class').substring(52), 'on', false)

patient_sort_down = driver.findElement(By.xpath('//*[@id="root"]/section/section/main/div/div/div/div/div/div/div/div/div[1]/table/thead/tr/th[9]/span/div/span[2]/div/i[2]'))

WebUI.verifyMatch(patient_sort_down.getAttribute('class').substring(56), 'off', false)

for (WebElement pointerValue : patientListTable) {
    WebUI.verifyMatch(pointerValue.findElement(By.xpath('.//td[9]')).getAttribute('class').substring(58), 'ant-table-column-sort', 
        false)
}

WebUI.click(findTestObject('page_patient_management/title_patientStatus'))

WebUI.delay(1)

patient_sort_up = driver.findElement(By.xpath('//*[@id="root"]/section/section/main/div/div/div/div/div/div/div/div/div[1]/table/thead/tr/th[9]/span/div/span[2]/div/i[1]'))

WebUI.verifyMatch(patient_sort_up.getAttribute('class').substring(52), 'off', false)

patient_sort_down = driver.findElement(By.xpath('//*[@id="root"]/section/section/main/div/div/div/div/div/div/div/div/div[1]/table/thead/tr/th[9]/span/div/span[2]/div/i[2]'))

WebUI.verifyMatch(patient_sort_down.getAttribute('class').substring(56), 'on', false)

for (WebElement pointerValue : patientListTable) {
    WebUI.verifyMatch(pointerValue.findElement(By.xpath('.//td[9]')).getAttribute('class').substring(58), 'ant-table-column-sort', 
        false)
}

