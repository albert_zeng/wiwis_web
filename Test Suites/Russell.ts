<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Russell</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>4167d1c1-c8e9-4175-a4fa-b401749e1b9b</testSuiteGuid>
   <testCaseLink>
      <guid>8730522d-b4f9-44fa-899d-3e7fa58cbc49</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/page_check/long_term_care_management/long_term_care_management</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b2076efe-e944-44e1-a394-613ca39ac3ef</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/page_check/menu_management/dressing_record_menu/dressing_management_increase</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8de961c4-8052-46ef-ae1f-69b44297d658</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/page_check/menu_management/patient_info_menu/discease_history_management</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c567d245-f238-4916-969f-41337f0cd045</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/page_check/menu_management/patient_info_menu/discease_history_switch</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>51a256cf-71f7-4bfc-b7bf-27a1bf140ae5</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>fd881574-dd60-4e23-8a5b-2cbcbde84b22</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>bca502cc-3db5-4bd7-a00e-6aec8df14aee</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/page_check/menu_management/patient_info_menu/patient_history_management</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>abe9f9d3-3759-43a9-8d63-371e42c5f3fb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/page_check/menu_management/patient_info_menu/patient_history_switch</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>0fad2686-10be-4c0b-a757-b9f6be868b4d</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>2fb35f53-4e63-45c2-b5e8-2124157e2886</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>6b979782-d735-44f2-b95e-15296921b1c3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/page_check/patient_management/patient_id_search</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9641c012-93cf-45b5-ab02-f5d923fe0554</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/page_check/patient_management/patient_list</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9d9a7784-2828-43f0-97aa-4a003ea6797d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/page_check/patient_management/patient_name_search</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
